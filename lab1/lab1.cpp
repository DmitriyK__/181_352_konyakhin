// Начало программы...Загрузим библиотеки
#include <iostream>
#include <bitset>
int main()
{
	// Добавим long long int для отображения числа
	long long int a = 260; char j = 'a'; bool{ true };
	// Найдем значение "a" в четвертой степени
	std::cout << "a*a*a*a=" << a * a*a*a << std::endl;
	// Найдем значение "а" в восьмой степени
	std::cout << "a*a*a*a*a*a*a*a=" << a * a*a*a*a*a*a*a << std::endl;
	// Определим значение переменной "а"и числа 37
	std::cout << "a+37=" << j + 37 << std::endl;
	// Определим значение числа 53 и переменной "a"
	std::cout << "53+a=" << 53 + j << std::endl;
	// Узнаем  длину в байтах переменных "int" и "bool"
	std::cout << sizeof(int) << std::endl;
	std::cout << sizeof(bool) << std::endl;
	// Добавим ф-ию ожидания
	getchar();
	return 0;
}
// Конец программы
