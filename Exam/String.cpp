#include "String.h"
#include <sstream>


using namespace std;


//����������� ��� ����������
String::String()
{
	str = nullptr;
	length = 0;
}

//����������� � �����������
String::String(const char *str)
{

	length = strlen(str);// ������� strlen �������� ���������� �������� � ������ ������� �� ������� � ������

	// �������� ������ ��� ������������� ������� ��� ����� ��������� ���� ������ 
	this->str = new char[length + 1]; // +1 ������ ��� ��� ����� ����� � ������� ��� ������������� 0

	// �������� ������� ������ � ������ ������ ������
	for (int i = 0; i < length; i++)
	{
		this->str[i] = str[i];
	}

	this->str[length] = '\0';//��������� ������

}

// ����������, �������� �� ������������ �������� ������� ��������, ���������� ��� ����������� ������� ������
String::~String()
{
	delete[] this->str;
}

// ����������� ����������� (��������� ��� �������� ������ ���� ������� ������ �� � ������ ������� ������) 
String::String(const String &other)
{
	length = strlen(other.str);
	this->str = new char[length + 1];

	for (int i = 0; i < length; i++)
	{
		this->str[i] = other.str[i];
	}

	this->str[length] = '\0';
}
// ����������� string
String::String(string _str)
{
	length = _str.length();
	this->str = new char[length + 1];
	for (int i = 0; i < length; i++)
	{
		this->str[i] = _str[i];
	}
	this->str[length] = '\0';
}



String& String::operator = (const String &other)// ������������ �������� ������������
{
	length = strlen(other.str);
	this->str = new char[length + 1];

	for (int i = 0; i < length; i++)
	{
		this->str[i] = other.str[i];
	}

	this->str[length] = '\0';

	return *this;

}

//������������� �������� ��������, � ������� ���������� ������ String ��������� ��� ������������ �����
String String::operator+(const String &other)
{
	//����� ������ ������ ��� ����� ������� ��������� ������������ ����� � ������� ����� ����������� ������
	String newStr;
	// �������� ���������� �������� � ����� ������� ��� ����������
	int thisLength = strlen(this->str);
	int otherLength = strlen(other.str);

	newStr.length = thisLength + otherLength;

	// �������� ����� � ������������ ������ ��� ����� ������
	newStr.str = new char[thisLength + otherLength + 1];

	int i = 0;
	for (; i < thisLength; i++) //�������� ������ � ����� ������
	{
		newStr.str[i] = this->str[i];
	}

	for (int j = 0; j < otherLength; j++, i++) //�������� ������ � ����� ������
	{
		newStr.str[i] = other.str[j];
	}

	newStr.str[thisLength + otherLength] = '\0'; // ����� ������

	// ���������� ��������� 
	return newStr;
}

void String::Print()
{
	for (int i = 0; i < length; i++)
		cout << str[i];
	cout << endl;
}

int String::Length()
{
	return length; // ������� ���������� �����
}

char& String::operator [](int index) // �������� ���������� ���������� ������
{
	return this->str[index];
}

void String::Clear() // ����� Clear
{
	if (str != nullptr)
	{
		delete[] str;
		str = new char[1];
		str[0] = 0;
		length = 0;
	}
}

void String::Add(string s) {//����� Add
	if (str != nullptr) {

		int count = s.length(), tmp;

		String n(str);

		delete[] str;
		str = new char[length + count + 1];
		tmp = length;
		length += count;
		count = tmp;

		for (int i = 0; i < count; i++)
			str[i] = n.str[i];

		for (int i = count; i < length; i++)
			str[i] = s[i - count];
		str[length] = '\0';
	}
}

void String::Insert(string s, int tor) //����� Insert
{
	if (str != nullptr && tor <= length && tor >= 0) {

		String b(str);

		length += s.length();

		delete[] str;
		str = new char[length + 1];

		for (int i = 0; i < tor; i++)
			str[i] = b.str[i];

		for (int i = tor; i < tor + s.length(); i++)
			str[i] = s[i - tor];

		for (int i = tor + s.length(); i < length; i++)
			str[i] = b.str[i - s.length()];

		str[length] = '\0';

	}
}

void String::Cut(int tor, int count) //����� Cut
{
	if (str != nullptr && tor + count <= length) {

		String n(str);

		length -= count;

		delete[] str;
		str = new char[length + 1];
		for (int i = 0; i < tor; i++)
			str[i] = n.str[i];
		for (int i = tor + count; i < n.length; i++)
			str[i - count] = n.str[i];
		str[length] = '\0';

	}
}

ostream & operator << (ostream & out, const  String& other) { // �������� << 
	for (int i = 0; i < other.length; i++)
	{
		out << other.str[i];
	}
	return out;
}

