#pragma once
#include <cstring>
#include <iostream>
#include <string>
#include "String.h"

using namespace std;
class String
{
private://������ � ���� ��� ����� ����� �������� ������ ��� � ��� �� ������ ��� ���������.
protected: //������ � ���� ��� ����� ����� �������� ������ ��� � ��� �� ������ ��� ��������� ��� � ����������� ������.
	char *str;
	int length;

public:
	String(string _str);
	String();//����������� ��� ����������
	String(const char *str); // �����������
	~String(); // ����������
	String(const String &other); // ����������� ����������� 
	//String(String &&other); //����������� ����������� 
	String& operator =(const String &other);// ������������ �������� ������������
	String operator+(const String &other);//������������� �������� ��������
	int Length();
	void Print();
	char& operator [](int index); // �������� ���������� ���������� ������
	void Clear();// ����� Clear 
	void Add(string s);
	void Insert(string s, int tor);
	void Cut(int tor, int count);
	string to_hex(unsigned char s);
	string sha256(string line);
	friend ostream & operator << (ostream & out, const  String& other);
	string CaesarEncrypt(string encText);
	string CaesarDecrypt(string encText);
};
