// Matrix
#include "pch.h"
#include <iostream>
#include <ctime>
#include <cassert>

using namespace std;

class matrix //class matrix
{
private://Доступ к типу или члену может получить только код в том же классе или структуре.
protected: //Доступ к типу или члену может получить только код в том же классе или структуре или в производном классе.
	int r, c;
	int** mat;

public:
	matrix()
	{
		mat = new int*[r];// динамическое выделение памяти под объект типа int
		for (int i = 0; i < r; i++)
		{
			mat[i] = new int[c];
		}
	}
	matrix(int row, int col) : r(row), c(col) // конструктор
	{
		r = row;
		c = col;
		srand(time(0));//используется для установки начала последовательности
		mat = new int*[r];// динамическое выделение памяти под объект типа int
		for (int i = 0; i < r; i++)
		{
			mat[i] = new int[c];
		}
	}

	void fill() //Создание рандомной матрици. Использовал для того, чтобы не вводить матрици в ручную;
	{
		for (int i = 0; i < this->r; i++)
		{
			for (int j = 0; j < this->c; j++)
			{
				this->mat[i][j] = rand() % 15 + 1;//Создаёт матрица из рандомных чисел
			}
		}
	}

	~matrix() //Деструктор
	{
		for (int i = 0; i < r; i++)
		{
			delete[] mat[i];
		}
		delete[] mat;
	}

	friend ostream & operator << (ostream & out, const matrix & m) { // Оператор << используется для вывода матриц в main (Потоковый вывод)
		for (int i = 0; i < m.r; i++)
		{
			for (int j = 0; j < m.c; j++) {
				out << m.mat[i][j] << "\t";
			}
			out << endl;
		}
		return out;
	}


	matrix operator + (const matrix& m) // оператор бинарного сложения 
	{
		matrix summ(m.r, m.c);// Удерживает значение
		for (int i = 0; i < m.r; i++)
		{
			for (int j = 0; j < m.c; j++)
			{
				summ.mat[i][j] = m.mat[i][j] + mat[i][j];
			}
		}
		return summ;
	}


	matrix operator * (const matrix& m) // Оператор умножения
	{
		matrix pr(m.r, m.c);// Удерживает значение
		for (int i = 0; i < m.r; i++)
		{
			for (int j = 0; j < m.c; j++)
			{
				pr.mat[i][j] = { };//Зануляю матрицу для вывода
				for (int n = 0; n < m.c; n++)
				{
					pr.mat[i][j] += m.mat[i][n] * mat[n][j];
				}
			}
		}
		return pr;
	}

	matrix operator = (const matrix& m)// Оператор присваивания
	{
		c = m.c;
		r = m.r;
		mat = new int*[r];
		for (int i = 0; i < r; i++)
		{
			mat[i] = new int[c];
		}

		for (int i = 0; i < r; i++)
		{
			for (int j = 0; j < c; j++)
			{
				mat[i][j] = m.mat[i][j];//присваивание
			}
		}
		return *this;
	}

	matrix(const matrix& m)// Конструктор копирования
	{
		c = m.c;
		r = m.r;

		mat = new int*[r];
		for (int i = 0; i < r; i++)
		{
			mat[i] = new int[c];
		}

		for (int i = 0; i < r; i++)
		{
			for (int j = 0; j < c; j++)
			{
				mat[i][j] = m.mat[i][j];
			}
		}

	}

	void show(matrix m) // show используется для вывода ответов в main
	{
		for (int i = 0; i < m.r; i++)
		{
			for (int j = 0; j < m.c; j++) {
				cout << m.mat[i][j] << "\t";//вывод mat с выравниванием табуляцией 
			}
			cout << endl;
		}
	}
};
class vec :public matrix //наследование class matrix -> class vec
{
public:
	vec() : matrix() {}

	vec(int row, int col) : matrix(row, col)
	{
		srand(time(0));//используется для установки начала последовательности
		mat = new int*[r];// динамическое выделение памяти под объект типа int
		for (int i = 0; i < r; i++)
		{
			mat[i] = new int[c];
		}
	}
	vec operator = (const vec& m)// Оператор присваивания
	{
		c = m.c;
		r = m.r;
		mat = new int*[r];
		for (int i = 0; i < r; i++)
		{
			mat[i] = new int[c];
		}

		for (int i = 0; i < r; i++)
		{
			for (int j = 0; j < c; j++)
			{
				mat[i][j] = m.mat[i][j];//присваивание
			}
		}
		return *this;
	}

	vec(const vec& m)// Конструктор копирования
	{
		c = m.c;
		r = m.r;

		mat = new int*[r];
		for (int i = 0; i < r; i++)
		{
			mat[i] = new int[c];
		}

		for (int i = 0; i < r; i++)
		{
			for (int j = 0; j < c; j++)
			{
				mat[i][j] = m.mat[i][j];
			}
		}

	}

	vec operator * (const vec& m)
	{
		vec v(m.r, m.c);// Удерживает значение
		for (int i = 0; i < m.r; i++)
		{
			for (int j = 0; j < m.c; j++)
			{
				v.mat[i][j] = {};
				v.mat[i][j] += m.mat[i][j] * mat[i][j];
			}
		}
		return v;
	}
	void show(vec m) // show используется для вывода ответов в main
	{
		for (int i = 0; i < m.r; i++)
		{
			for (int j = 0; j < m.c; j++) {
				cout << m.mat[i][j] << " ";//вывод mat с выравниванием табуляцией 
			}
		}
	}
};

int main()
{
	setlocale(LC_ALL, "Russian");
	int row, col;
	cout << "Введите кол-во строк: " << endl;
	cin >> row;
	cout << "Введите кол-во колонок: " << endl;
	cin >> col;
	cout << endl;

	cout << "Матрица A: " << endl;
	matrix m(row, col);
	m.fill();
	cout << m << endl;

	cout << "Введите кол-во строк: " << endl;
	cin >> row;
	cout << "Введите кол-во колонок: " << endl;
	cin >> col;
	cout << endl;

	cout << "Матрица B: " << endl;
	matrix m2(row, col);
	m2.fill();
	cout << m2 << endl;

	cout << endl << "----------" << endl;
	cout << "Сумма" << endl;//Cложение
	matrix m3(row, col);
	m3 = m + m2;
	m3.show(m3);

	cout << endl << "----------" << endl;
	cout << "Произведение" << endl;//Умножение
	matrix h3(row, col);
	h3 = m * m2;
	h3.show(h3);

	cout << endl << "Длина вектора А: " << endl;
	cin >> row;
	col = 1;
	cout << "Вектор А: " << endl;
	vec w1(row, col);
	w1.fill();
	w1.show(w1);
	cout << endl;

	cout << endl << "Длина вектора В: " << endl;
	col = 1;
	cin >> row;
	cout << "Вектор B: " << endl;

	vec w2(row, col);
	w2.fill();
	w2.show(w2);

	cout << endl << "----------" << endl;
	cout << "Скалярное произведение:" << endl;//Умножение
	vec w3(row, col);
	w3 = w1 * w2;
	w3.show(w3);

	getchar();
	return 0;
}