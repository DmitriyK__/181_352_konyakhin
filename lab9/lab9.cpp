
#include <iostream>
#include <string> //работа со строками
#include <queue> //контейнер реализует модель очереди
#include <vector> //необходимо для использования вектора
#include <array> //фиксированный массив
#include <stack> //необходимо для использования стека
#include <list> //быстрая вставка и удаление элементов из любой позиции контейнера
#include <map> //контейнер сразу для нескольких значений 
#include <tuple> //кортеж, неоднородный список, типы которых задаются на этапе компиляции 
#include <Windows.h> //нужен для совместимости с win старых версий 
using namespace std;

class Tree {
private: //доступ открыт самому классу и друзьям данного класса, как функциям, так и классам
	char Data; // обьем данных
	Tree *LeftTree; // левый потомок
	Tree *RightTree; // правый потомок

public://доступ открыт всем, кто видит определение данного класса
	void addtree(char data, Tree *&node); // Добавление узла к дереву
	void print(Tree *node); //Сортировка и вывод
	void FreeTree(Tree *node); // Удаления левого и правого дерева
};


// Функция предназначенная для добавления узла к дереву
void Tree::addtree(char data, Tree *&node)
{
	if (!node) //Пока не будет найден пустой корень дерева 
	{
		node = new Tree;  //Выделяем память
		node->Data = data; //Перемещаем в выделенную память  Data
		node->LeftTree = 0; // Добавляем к корню дерева левую сторону
		node->RightTree = 0; // Добавляем к корню дерева правую сторону
		return;
	}
	else
		if (node->Data > data)
			addtree(data, node->LeftTree); //Рекурсивная функция для левого поддерева
		else
			addtree(data, node->RightTree);//Рекурсивная функция для правого поддерева
	//рекурсивная функция - это функция, которая вызывает саму себя(прямая рекурсия)
	//(косвенная рекурсия) - функции вызывают друг друга 
}

int kv, kr, fig;
bool flag = true;


void Tree::print(Tree *node)// Функция на саму сортировку
{
	if (!node) return; // Выход при отсутствии ветки

	print(node->LeftTree); // Выводим ветки, только те, что находятся слева 

	if (node->Data == '[')
	{
		kv++;
		cout << kv;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == ']')
	{
		if (kv == 0) flag = false;
		kv--;
		cout << kv;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == '{')
	{
		fig++;
		cout << fig;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == '}')
	{
		if (fig == 0) flag = false;
		fig--;
		cout << fig;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == '(')
	{
		kr++;
		cout << kr;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == ')')
	{
		if (kr == 0) flag = false;
		kr--;
		cout << kr;
		cout << node->Data << endl; // Данные ветки
	}

	print(node->RightTree); //Выводим ветки, только те, что находятся справа

	return;
}
void Tree::FreeTree(Tree *node) // Удаление правого и левого поддерева
{
	if (!node) return; // Выход при отсутствии ветки
	FreeTree(node->LeftTree);
	FreeTree(node->RightTree);
	delete node;
	return;
}

int main() {
	setlocale(LC_ALL, "RUS");
	Tree *Node = NULL; // Корень
	Tree b;
	string a;
	cout << "Enter brackets: " << endl;
	a = "{({})}";
	cout << a << endl;

	for (int i = 0; i < a[i]; i++)
	{
		b.addtree(a[i], Node);
	}

	cout << "Sorting brackets: " << endl;
	b.print(Node); // вывод узла
	b.FreeTree(Node); // удаление узла

	
	cout << "Result: " << endl;
	if ((kv != 0 || kr != 0 || fig != 0) || (flag == false))
	{
		cout << "[False]" << endl;
	}
	else
	{
		cout << "[True]" << endl;
	}
	cout << endl;

	system("pause");
	return 0;
}