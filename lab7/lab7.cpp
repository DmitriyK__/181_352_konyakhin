﻿// Самостоятельные задания:
#include <iostream>
#include <string>
#include <queue>  
#include <vector>
#include <array>
#include <stack>
#include <map>
#include <tuple>
#include <deque>
#include <list>
#include <set>
#include <iomanip>
#include<numeric>
#include<iterator>
#include <algorithm>
#include <cstdlib>

using namespace std;

//// 1) Проверка правильности введения скобок
//
//int main()
//{
//	setlocale(0, "rus");
//	string s;
//	cout << "Введите пример для проверки:" << endl;
//	getline(cin, s);
//	stack<char> stk;
//	char a = 0;
//	stk.push(s[0]);
//
//	for (int i = 1; i < s.size(); i++) // так как 0 уже в стеке начинаем с 1-и
//	{
//		if (s[i] == '(' || s[i] == '[' || s[i] == '{') // Если записывается ( || [ || { то они помещаются в stack
//		{
//			stk.push(s[i]);
//		}
//		else if (s[i] == ')')
//		{
//			if (stk.size() > 0 && (stk.top() == '('))
//				stk.pop();
//		}
//		else if (s[i] == ']')
//		{
//			if (stk.size() > 0 && (stk.top() == '['))
//				stk.pop();
//		}
//		else if (s[i] == '}')
//		{
//			if (stk.size() > 0 && (stk.top() == '{'))
//				stk.pop();
//		}
//	}
//	if (stk.empty())
//	{
//		cout << "<<Stack is empty>>" << endl;
//		cout << "<<True>>" << endl; // выводится при правильном порядке скобок
//	}
//	else
//	{
//		cout << "<<Stack is not empty>>" << endl;
//		cout << "<<False>>" << endl;// выводится при не правильном  порядке скобок
//	}
//}
//
//// 2) Вывод по классам
//
//int main()
//{
//	setlocale(0, "rus");
//	cout << "При завершении списка введите 0: " << endl;
//	queue <string> cl_9, cl_10, cl_11;
//	string Fam;
//	int cl;
//	cin >> cl;
//	for (;;)
//	{
//		getline(cin, Fam);
//		if (cl == 0)
//		{
//			break;
//		}
//		else if (cl == 9)
//		{
//			cl_9.push(Fam);
//		}
//		else if (cl == 10)
//		{
//			cl_10.push(Fam);
//		}
//		{
//			cl_11.push(Fam);
//		}
//
//
//		cin >> cl;
//	}
//	cout << "<<Распределение выполнено>>" << endl;
//	cout << "Class 9:" << endl;
//	while (!cl_9.empty())
//	{
//		cout << cl_9.front() << endl;
//		cl_9.pop();
//	}
//	cout << endl << "Class 10:" << endl;
//	while (!cl_10.empty())
//	{
//		cout << cl_10.front() << endl;
//		cl_10.pop();
//	}
//	cout << endl << "Class 11:" << endl;
//	while (!cl_11.empty())
//	{
//		cout << cl_11.front() << endl;
//		cl_11.pop();
//	}
//	cout << "-----------" << endl;
//	system("pause");
//	return 0;
//}
//
//// 3) Дек. Полиндром.
//
//int main()
//{
//	setlocale(0, "rus");
//	string s;
//	cout << "<<Проверка на палидромность>> \nВвод: " << endl;
//	getline(cin, s);
//	deque<int> deq;
//	for (int i = 0; i < s.length(); i++)
//	{
//		deq.push_front(s[i]);
//	}
//
//	for (int i = 0; i < s.length(); i++)
//	{
//		if ((deq.size() > 1) && deq.front() == deq.back())
//		{
//			deq.pop_front();
//			deq.pop_back();
//		}
//		else if (deq.empty() || deq.size() == 1)
//		{
//			cout << "Палиндром" << endl;
//			break;
//		}
//		else
//		{
//			cout << "Не палиндром" << endl;
//			break;
//		}
//	}
//}
//
//// 4) List.
//
//int main()
//{
//	setlocale(0, "RUS");
//	cout << "Первоначальный список:" << endl;
//	list <int> this_list = { 7, 4, 6, 3, 3, 2 };
//	copy(this_list.begin(), this_list.end(), ostream_iterator<int>(cout, " ")); // this_list.begin(), this_list.end() - это итераторы начала и вывода, итератор вывода — ostream_iterator<int>(cout," ")
//	cout << endl;
//
//	cout << endl << "Добавляем в начало и конец цифры:" << endl;
//	this_list.push_front(5);
//	this_list.push_back(1);
//	copy(this_list.begin(), this_list.end(), ostream_iterator<int>(cout, " ")); // this_list.begin(), this_list.end() - это итераторы начала и вывода, итератор вывода — ostream_iterator<int>(cout," ")
//	cout << endl;
//}
//
//// 5) Map
//
//int main()
//{
//	setlocale(LC_ALL, "RUS");
//	map<string, string> map1;
//	map1["T-80"] = "USSR";
//	map1["Merkava"] = "Israel";
//	map1["M1 Abrams"] = "USA";
//
//	cout << map1["T-80"] << endl;
//	cout << map1["Merkava"] << endl;
//	cout << map1["M1 Abrams"] << endl << endl;
//
//	map <int, int> mp;
//
//	cout << "Введите количество элементов: ";
//	int n;
//	cin >> n;
//
//	for (int i = 0; i < n; i++) {
//		cout << "Номер " << i << ") ";
//		int a; cin >> a;
//		mp[a] = i;  // добавляем новые элементы
//	}
//
//	map <int, int> ::iterator it = mp.begin();
//	cout << "Сортировка: " << endl;
//	for (int i = 0; it != mp.end(); it++, i++) // выводим их
//	{
//		cout << "Номер " << i << ") | Начальный номер " << it->second << ") | " << it->first << endl;
//	}
//
//
//	system("pause");
//	return 0;
//}
// 
