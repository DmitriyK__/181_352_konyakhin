
#include <iostream>
#include <ctime>
#include <cassert>

using namespace std;

class matrix //class matrix
{
private://Доступ к типу или члену может получить только код в том же классе или структуре.
protected: //Доступ к типу или члену может получить только код в том же классе или структуре или в производном классе.
	int size;
	int** mat;

public:
	
	matrix()
	{

	}

	matrix(int Size) // конструктор
	{
		size = Size;
		mat = new int*[Size];// динамическое выделение памяти под объект 
		for (int i = 0; i < Size; i++)
		{
			mat[i] = new int[Size];
		}
	}

	void fill()// ввод матрицы
	{
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				cin >> mat[i][j];
			}
		}
	}

	~matrix() //Деструктор
	{
		for (int i = 0; i < size; i++)
		{
			delete[] mat[i];
		}
		delete[] mat;
	}

	friend ostream & operator << (ostream & out, const matrix & m) { // Оператор << используется для вывода матриц в main (Потоковый вывод)
		for (int i = 0; i < m.size; i++)
		{
			for (int j = 0; j < m.size; j++) {
				out << m.mat[i][j] << "\t";
			}
			out << endl;
		}
		return out;
	}


	matrix operator + (const matrix& m) // оператор бинарного сложения 
	{
		matrix summ(m.size);// Удерживает значение
		for (int i = 0; i < m.size; i++)
		{
			for (int j = 0; j < m.size; j++)
			{
				summ.mat[i][j] = m.mat[i][j] + mat[i][j];
			}
		}
		return summ;
	}


	matrix operator * (const matrix& m) // Оператор умножения
	{
		matrix pr(m.size);// Удерживает значение
		for (int i = 0; i < m.size; i++)
		{
			for (int j = 0; j < m.size; j++)
			{
				pr.mat[i][j] = { };//Зануляю матрицу для вывода
				for (int n = 0; n < m.size; n++)
				{
					pr.mat[i][j] += m.mat[i][n] * mat[n][j];
				}
			}
		}
		return pr;
	}

	matrix operator = (const matrix& m)//перегрузка оператора присваивания
	{
		size = m.size;
		mat = new int*[size];
		for (int i = 0; i < size; i++)
		{
			mat[i] = new int[size];
		}

		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				mat[i][j] = m.mat[i][j];//присваивание
			}
		}
		return *this;
	}

	matrix(const matrix& m)// Конструктор копирования
	{
		size = m.size;
		mat = new int*[size];
		for (int i = 0; i < size; i++)
		{
			mat[i] = new int[size];
		}

		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				mat[i][j] = m.mat[i][j];
			}
		}
	}

	void show(matrix m) // show используется для вывода ответов в main
	{
		for (int i = 0; i < m.size; i++)
		{
			for (int j = 0; j < m.size; j++) {
				cout << m.mat[i][j] << "\t";//вывод mat с выравниванием табуляцией 
			}
			cout << endl;
		}
	}
};
class vec :public matrix //наследование class matrix -> class vec
{
public:
	vec() : matrix() {}

	vec(int Size) : matrix(Size)
	{
		mat = new int*[size];// динамическое выделение памяти под объект 
		for (int i = 0; i < size; i++)
		{
			mat[i] = new int[size];
		}
	}
	vec operator = (const vec& m)// Оператор присваивания
	{
		size = m.size;
		mat = new int*[size];
		for (int i = 0; i < size; i++)
		{
			mat[i] = new int[size];
		}

		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				mat[i][j] = m.mat[i][j];//присваивание
			}
		}
		return *this;
	}

	vec(const vec& m)// Конструктор копирования
	{
		size = m.size;

		mat = new int*[size];
		for (int i = 0; i < size; i++)
		{
			mat[i] = new int[size];
		}

		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				mat[i][j] = m.mat[i][j];
			}
		}

	}

	vec operator * (const vec& m)//оператор умножения.
	{
		vec v(m.size);// Удерживает значение.
		for (int i = 0; i < m.size; i++)
		{
			for (int j = 0; j < m.size; j++)
			{
				v.mat[i][j] = {};
				v.mat[i][j] += m.mat[i][j] * mat[i][j];
			}
		}
		return v;
	}

	void fillvec()// ввод вектора 
	{
		for (int i = 0; i < size; i++)
		{
			for (int j = 1; j < 2; j++)
			{
				cin >> mat[i][j];
			}
		}
	}

	void Vshow()// вывод вектора
	{
		for (int i = 0; i < size; i++)
		{
			for (int j = 1; j < size; j++)
			{
				cout << mat[i][j];
			}
		}
	}

};

int main()
{
	setlocale(LC_ALL, "Russian");
	int size;
	cout << "Размер матрицы A: " << endl;
	cin >> size;

	cout << "Матрица A: " << endl;
	matrix m(size);
	m.fill();

	cout << "Размер матрицы B: " << endl;
	cin >> size;
	cout << endl;

	cout << "Матрица B: " << endl;
	matrix m2(size);
	m2.fill();

	cout << endl << "----------" << endl;
	cout << "Сумма" << endl;//Cложение
	matrix m3(size);
	m3 = m + m2;
	m3.show(m3);

	cout << endl << "----------" << endl;
	cout << "Произведение" << endl;//Умножение
	matrix h3(size);
	h3 = m * m2;
	h3.show(h3);

	//cout << endl << "Длина вектора А: " << endl;
	//cin >> size;

	cout << "Введите вектор А: " << endl;
	vec w1(2);
	w1.fillvec();


	/*cout << endl << "Длина вектора В: " << endl;
	cin >> size;*/

	cout << "Введите вектор B: " << endl;
	vec w2(2);
	w2.fillvec();

	cout << endl << "----------" << endl;
	cout << "Скалярное произведение:";//Умножение
	vec w3(size);
	w3 = w1 * w2;
	w3.Vshow();
	cout << '\n';
	getchar();
	system("Pause");
	return 0;

}

