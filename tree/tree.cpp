#include <iostream>
#include <string> //������ �� ��������
#include <queue> //��������� ��������� ������ �������
#include <vector> //���������� ��� ������������� �������
#include <array> //������������� ������
#include <stack> //���������� ��� ������������� �����
#include <list> //������� ������� � �������� ��������� �� ����� ������� ����������
#include <map> //��������� ����� ��� ���������� �������� 
#include <tuple> //������, ������������ ������, ���� ������� �������� �� ����� ���������� 
#include <Windows.h> //����� ��� ������������� � win ������ ������ 
using namespace std;

class Tree {
private: //������ ������ ������ ������ � ������� ������� ������, ��� ��������, ��� � �������
	char Data; // ����� ������
	Tree *LeftTree; // ����� �������
	Tree *RightTree; // ������ �������

public://������ ������ ����, ��� ����� ����������� ������� ������
	void addtree(char data, Tree *&node); // ���������� ���� � ������
	void print(Tree *node); //���������� � �����
	void FreeTree(Tree *node); // �������� ������ � ������� ������
};


// ������� ��������������� ��� ���������� ���� � ������
void Tree::addtree(char data, Tree *&node)
{
	if (!node) //���� �� ����� ������ ������ ������ ������ 
	{
		node = new Tree;  //�������� ������
		node->Data = data; //���������� � ���������� ������  Data
		node->LeftTree = 0; // ��������� � ����� ������ ����� �������
		node->RightTree = 0; // ��������� � ����� ������ ������ �������
		return;
	}
	else
		if (node->Data > data)
			addtree(data, node->LeftTree); //����������� ������� ��� ������ ���������
		else
			addtree(data, node->RightTree);//����������� ������� ��� ������� ���������
	//����������� ������� - ��� �������, ������� �������� ���� ����(������ ��������)
	//(��������� ��������) - ������� �������� ���� ����� 
}

int kv, kr, fig;
bool flag = true;


void Tree::print(Tree *node)// ������� �� ���� ����������
{
	if (!node) return; // ����� ��� ���������� �����

	print(node->LeftTree); // ������� �����, ������ ��, ��� ��������� ����� 

	if (node->Data == '[')
	{
		kv++;
		cout << kv;
		cout << node->Data << endl; // ������ �����
	}
	else if (node->Data == ']')
	{
		if (kv == 0) flag = false;
		kv--;
		cout << kv;
		cout << node->Data << endl; // ������ �����
	}
	else if (node->Data == '{')
	{
		fig++;
		cout << fig;
		cout << node->Data << endl; // ������ �����
	}
	else if (node->Data == '}')
	{
		if (fig == 0) flag = false;
		fig--;
		cout << fig;
		cout << node->Data << endl; // ������ �����
	}
	else if (node->Data == '(')
	{
		kr++;
		cout << kr;
		cout << node->Data << endl; // ������ �����
	}
	else if (node->Data == ')')
	{
		if (kr == 0) flag = false;
		kr--;
		cout << kr;
		cout << node->Data << endl; // ������ �����
	}

	print(node->RightTree); //������� �����, ������ ��, ��� ��������� ������

	return;
}
void Tree::FreeTree(Tree *node) // �������� ������� � ������ ���������
{
	if (!node) return; // ����� ��� ���������� �����
	FreeTree(node->LeftTree);
	FreeTree(node->RightTree);
	delete node;
	return;
}

int main() {
	setlocale(LC_ALL, "RUS");
	Tree *Node = NULL; // ������
	Tree b;
	string a;
	cout << "Enter brackets: " << endl;
	a = "{({})}";
	cout << a << endl;

	for (int i = 0; i < a[i]; i++)
	{
		b.addtree(a[i], Node);
	}

	cout << "Sorting brackets: " << endl;
	b.print(Node); // ����� ����
	b.FreeTree(Node); // �������� ����


	cout << "Result: " << endl;
	if ((kv != 0 || kr != 0 || fig != 0) || (flag == false))
	{
		cout << "[False]" << endl;
	}
	else
	{
		cout << "[True]" << endl;
	}
	cout << endl;

	system("pause");
	return 0;
}