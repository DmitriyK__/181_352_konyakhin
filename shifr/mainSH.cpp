#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <openssl/conf.h> // �������, ��������� � ��������� ��������� OpenSSL
#include <openssl/conf.h>
#include <openssl/evp.h> 
#include <openssl/err.h> // ���� ���������� ������ OpenSSL � �� �����������
#include <openssl/aes.h>
#include <fstream>

#pragma comment (lib, "ws2_32.LIB")
#pragma comment (lib, "gdi32.LIB")
#pragma comment (lib, "advapi32.LIB")
#pragma comment (lib, "crypt32")
#pragma comment (lib, "user32")
#pragma comment (lib, "wldap32")


using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");

	unsigned char *key = (unsigned char *)"01234567890123456789012345678901"; // ������ (����)
	unsigned char *iv = (unsigned char *)"0123456789012345"; // ���������������� ������, �����������
	unsigned char cryptedtext[256]; // ������������� ���������
	unsigned char decryptedtext[256]; // �������������� ���������
	unsigned char buff[256];

	fstream first("original_file.txt", ios::binary | ios::in); // �������� ����� � �������, ������� ����� �����������
	fstream crypted("crypted_file.txt", ios::binary | ios::out | ios::in | ios::trunc); // �������� �����, � ������� ������� ������������� �����
	fstream decrypted("decrypted_file.txt", ios::binary | ios::out | ios::trunc); // �������� �����, � ������� ������� �������������� ���������

	EVP_CIPHER_CTX *ctx;
	ctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);

	int len = 0;
	int num_bytes_read;

	first.read((char*)buff, 256);
	while (first.gcount() > 0)
	{
		EVP_EncryptUpdate(ctx,
			cryptedtext,
			&len,
			buff,
			first.gcount());
		crypted.write((char*)cryptedtext, len);
		first.read((char*)buff, 256);
	}
	EVP_EncryptFinal_ex(ctx, cryptedtext, &len);
	crypted.write((char*)cryptedtext, len);
	crypted.close();
	first.close();


	EVP_CIPHER_CTX_free(ctx);

	//// ����� ������������� ������
	//for (int i = 0; i < len; i++)
	//{
	//	cout << hex << cryptedtext[i];
	//	if ((i + 1) % 32 == 0) cout << endl;
	//}
	//cout << endl;

	// �����������
	crypted.open("crypted_file.txt", ios::in | ios::binary);

	ctx = EVP_CIPHER_CTX_new(); // �������� ��������� � ����������� ������
	EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // ������������� ������� AES, ������ � ��������
	len = 0;
	crypted.read((char*)cryptedtext, 256);
	while (crypted.gcount() > 0)
	{
		EVP_DecryptUpdate(ctx, decryptedtext, &len, cryptedtext, crypted.gcount());
		decrypted.write((char*)decryptedtext, len);
		crypted.read((char*)cryptedtext, 256);
		/*cout << decryptedtext << endl;*/
	}

	EVP_DecryptFinal_ex(ctx, decryptedtext, &len);
	decrypted.write((char*)decryptedtext, len);
	decrypted.close();
	crypted.close();

	//decrypted_len += len;
	EVP_CIPHER_CTX_free(ctx);
	decryptedtext[len] = '\0';
	//cout << decryptedtext << endl;


	getchar();
	return 0;
}
